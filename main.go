package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/aws-test/domain"
)

const Billion = 1000000000

func main() {
	if err := fasthttp.ListenAndServe(fmt.Sprint(":", os.Getenv("PORT")), responseStreamHandler); err != nil {
		log.Fatalf("unexpected error in server: %s", err)
	}
}

func responseStreamHandler(ctx *fasthttp.RequestCtx) {
	// Send the response in chunks and wait for a second between each chunk.
	ctx.SetBodyStreamWriter(func(w *bufio.Writer) {
		for i := 0; i < Billion; i++ {
			msg := domain.Message{
				Text:      "hello_world",
				ContentID: strconv.Itoa(i),
				ClientID:  "5",
				Timestamp: time.Now().Format("2006-01-02"),
			}
			data, err := json.Marshal(&msg)
			if err != nil {
				return
			}
			_, err = bytes.NewBuffer(data).WriteTo(w)
			if err != nil {
				return
			}
			if err := w.Flush(); err != nil {
				return
			}
		}
	})
}
