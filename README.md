# aws-test-client

Setup: 
- git clone https://gitlab.com/IvanSmaliakou/aws-test-client.git
- go get -u ./...
- export PORT={PORT on which the service will run (have to be equal to port of the server)}

Run:
- go run main.go

Then:
- Follow instructions on server https://gitlab.com/IvanSmaliakou/aws-test